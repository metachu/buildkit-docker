FROM moby/buildkit:v0.7.1
RUN apk add --update-cache \
    docker \
    #curl \
    make \
    #wget \
    git \
    #unzip \
    #python \
    #python-dev \
    #py-pip \
    #build-base \
  && rm -rf /var/cache/apk/*
#RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip && ./aws/install

ENV DOCKER_BUILDKIT=1

ENTRYPOINT ["/bin/sh", "-c"]
