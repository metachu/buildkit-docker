CONTAINER_IMAGE?=registry.gitlab.com/metachu/buildkit-docker
IMAGE_LATEST?=${CONTAINER_IMAGE}:latest
IMAGE_CACHE=${CONTAINER_IMAGE}:cache
IMAGE?=${IMAGE_LATEST}
BUILD_IMAGE=${LATEST_IMAGE}

build:
	@echo "building ${IMAGE}"
	docker pull ${IMAGE_LATEST} || true
	docker build --cache-from ${IMAGE_LATEST} -t ${IMAGE} .

push:
	@echo "pushing ${IMAGE}"
	docker push ${IMAGE}

docker/shell:
	docker run --rm -it --privileged -v `pwd`:/code/ ${IMAGE_LATEST} sh

buildkit_build:
	@echo "building and releasing ${IMAGE}"
	buildctl-daemonless.sh build \
		--frontend=dockerfile.v0 \
		--local context=. \
		--local dockerfile=. \
		--output type=image,name=${IMAGE},push=true \
		--export-cache type=registry,ref=${IMAGE_CACHE}\
		--import-cache type=registry,ref=${IMAGE_CACHE}
